import { createBrowserRouter } from "react-router-dom";
import Home from "./routes/Home";
import ColorGame from "./routes/ColorGame";
import Doable from "./routes/Doable";

import App from "./routes/App/App";
import Unauthenticated from "./components/Unauthenticated";
import Authenticated, {loader as authenticatedLoader } from "./components/Authenticated";
import PrivateRoute from "./routes/PrivateRoute/PrivateRoute";


export const router = createBrowserRouter([
    {
        id:"app",
        path: "/",
        element:<App></App>,
        children:[
            {
                index:true,
                element: <Home></Home>
            },
            {
                path:"color-game",
                element: <ColorGame></ColorGame>
            },
            {
                path:"doable",
                element: <Doable></Doable>,
                children:[
                    {
                        index:true,
                        element: <Unauthenticated></Unauthenticated>

                    },
                    {
                        path:"notes",
                        loader:authenticatedLoader,
                        element: <PrivateRoute >
                            <Authenticated></Authenticated>
                        </PrivateRoute>
                        
                    }
                ]
            }
        ]
    }
])