// @vitest-environment jsdom

import { describe, it, expect, beforeEach } from "vitest";
import { render, screen, fireEvent } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import ColorGame from "./ColorGame";

describe("ColorGame Component", () => {
  beforeEach(() => {
    render(<ColorGame />);
  });

  it("renders without crashing", () => {
    const colorGameElement = screen.queryByText("Color Game");
    expect(colorGameElement).not.toBeNull();
  });
  it("should render 3 color squares after clicking the reset button", async () => {
    const colorSquares =  screen.getAllByTestId("color-square");
    expect(colorSquares.length).toBe(6);
  });

  it("starting value of the number of colors", async () => {
    const numberInput = screen.getByLabelText("# Colors");
    expect(numberInput.value).toBe("3");
  });

  it("changes the number of colors using the incrementor", async () => {
    const numberInput = screen.getByLabelText("# Colors");
    fireEvent.change(numberInput, { target: { value: "6" } });
    expect(numberInput.value).toBe("6");
  });
});
