import s from "./ColorGame.module.css";
import { useState, useEffect, useMemo } from "react";
import { getRandomColors, rgbString, statusMessage } from "./utils";

function ColorGame() {
  const [red, setRed] = useState(255);
  const [blue, setBlue] = useState(255);
  const [green, setGreen] = useState(255);
  const [numOfColors, setNumOfColors] = useState(3);
  const [arrayColors, setArrayColors] = useState([]);
  const [colorStates, setColorStates] = useState([]);
  const [attempts, setAttempts] = useState([]);
  const [status, setStatus] = useState("playing");
  const [isGameFinished, setIsGameFinished] = useState(false);



  useEffect(()=>{
    const numero1 = Math.floor(Math.random() * 256);
    const numero2 = Math.floor(Math.random() * 256);
    const numero3 = Math.floor(Math.random() * 256);
    setRed(numero1);
    setGreen(numero2);
    setBlue(numero3);
    
  },[])

  const target = useMemo(() => {
    return Math.floor(Math.random() * numOfColors);
  }, [numOfColors]);

  useEffect(() => {
    setArrayColors(getRandomColors(numOfColors, target, [red, green, blue]));
    setColorStates(Array(numOfColors).fill(true));
  }, [numOfColors, target, red, green, blue]);

  useEffect(() => {

      

  }, [isGameFinished]);

  useEffect(() => {
    if (status === "lose" && !isGameFinished) {
      const arrayWin = [...Array(arrayColors.length)].map(() => [
        red,
        green,
        blue,
      ]);
      setColorStates(Array(numOfColors).fill(true));
      setArrayColors(arrayWin);
    }
  }, [status, arrayColors, numOfColors, red, green, blue, isGameFinished]);

  function handleReset() {
    setAttempts([]);
    const nuevosColores = getRandomColors(numOfColors, target, [
      red,
      green,
      blue,
    ]);
    setArrayColors(nuevosColores);
    setColorStates(Array(numOfColors).fill(true));

    const numero1 = Math.floor(Math.random() * 256);
    const numero2 = Math.floor(Math.random() * 256);
    const numero3 = Math.floor(Math.random() * 256);
    setRed(numero1);
    setGreen(numero2);
    setBlue(numero3);
    setIsGameFinished(false);
    setStatus("playing");
  }

  const estiloRed = {
    border: `8px solid rgb(${red},0,0)`,
  };

  const estiloGreen = {
    border: `8px solid rgb(0,${green},0)`,
  };

  const estiloBlue = {
    border: `8px solid rgb(0,0,${blue})`,
  };

  function handleChangeNumber(event) {
    let nuevoValue = event.target.value;
    if (isNaN(nuevoValue)) {
      nuevoValue = 3;
    } else {
      if (nuevoValue <= 4) {
        nuevoValue = 3;
      } else if (nuevoValue <= 7) {
        nuevoValue = 6;
      } else {
        nuevoValue = 9;
      }
    }
    setNumOfColors(parseInt(nuevoValue));
    setAttempts([]);
    setIsGameFinished(false);
    setStatus("playing");
  }

  function handleOnClick(index) {
    if (isGameFinished) {
      return;
    }
    if (index === target) {
      const arrayWin = [...Array(arrayColors.length)].map(
        () => arrayColors[index]
      );
      setColorStates(Array(numOfColors).fill(true));
      setArrayColors(arrayWin);
      setIsGameFinished(true);
      setStatus("win");
    } else {
      const updatedStates = [...colorStates];
      updatedStates[index] = false;
      setColorStates(updatedStates);
      setAttempts([...attempts, index]);

      const maxAttempts = numOfColors;
      console.log("maximo :", maxAttempts);
      console.log(attempts.length);
      if (attempts.length + 2 === maxAttempts) {
        setIsGameFinished(true);
        setStatus("lose");
        const arrayWin = [...Array(arrayColors.length)].map(
          () => [red,green,blue]
        );
        setColorStates(Array(numOfColors).fill(true));
        setArrayColors(arrayWin);
      }
    }
  }

  return (
    <div className={s.wrapper}>
      <h1 className={s.title}>Color Game</h1>
      <p className={s.description}>
        Guess which color correspond to the following RGB code
      </p>
      <div className={s.container__componente}>
        <div className={s.color}>
          <div style={estiloRed}>
            <div className={s.value}>
              <p>{red}</p>
            </div>
            <div>
              <p>red</p>
            </div>
          </div>
          <div style={estiloGreen}>
            <div className={s.value}>
              <p>{green} </p>
            </div>
            <div>
              <p>green</p>
            </div>
          </div>
          <div style={estiloBlue}>
            <div className={s.value}>
              <p>{blue} </p>
            </div>
            <div>
              <p>blue</p>
            </div>
          </div>
        </div>
      </div>
      <div className={s["rgb-wrapper"]}>{rgbString([red, green, blue])}</div>
      <div className={s.dashboard}>
        <div className={s["number-input"]}>
          <label htmlFor="colors"># Colors</label>
          <input
            id="colors"
            type="number"
            placeholder="3"
            onChange={handleChangeNumber}
            step={3}
            min={3}
            max={9}
          />
        </div>
        <p className={s["game-status"]}>{statusMessage[status]}</p>
        <button onClick={handleReset} data-testid="boton-reset">Reset</button>
      </div>
      <div className={s.squares}>
        {arrayColors.map((color, index) => {
          const backgroundColor = rgbString(color);
          const opacity = colorStates[index] ? "100" : "0";

          return (
            <button
              key={index}
              style={{ backgroundColor, opacity }}
              onClick={() => {
                handleOnClick(index);
              }}
              className={s.square}
              data-testid="color-square"
            ></button>
          );
        })}
      </div>
    </div>
  );
}

export default ColorGame;
