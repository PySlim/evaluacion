import s from "./Doable.module.css";
// import Authenticated from "../../components/Authenticated";
// import Unauthenticated from "../../components/Unauthenticated"
import { Outlet } from "react-router-dom";


function Doable() {
  // const isAuthenticated = true;

  return (
    <div className={s.wrapper}>
      <h1 className={s.title}>Doable</h1>
      <p className={s.description}>Add and filter your most important tasks</p>
      {/* {isAuthenticated ? <Authenticated /> : <Unauthenticated />} */}
      <Outlet></Outlet>
    </div>
  );
}

export default Doable;
