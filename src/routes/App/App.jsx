import Header from "../../components/Header";
import s from "./App.module.css";

import { Outlet } from "react-router-dom";

function App() {
  return (
    <div className={s.div}>
      <Header></Header>
      <main>
        <Outlet></Outlet>
      </main>
    </div>
  );
}

export default App;
