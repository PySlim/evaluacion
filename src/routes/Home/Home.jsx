import s from "./Home.module.css";
import reactIconUrl from "../../../public/assets/react-icon-lg.svg";

import { Link } from "react-router-dom";

function Home() {
  return (
    <div className={s.wrapper}>
      <img src={reactIconUrl} />
      <h1 className={s.title}>React Evaluation</h1>
      <p className={s.name}>Slim Josh Zegarra Soto</p>
      <div className={s.buttons}>
        <Link to="/color-game" className={s.elementLink}>
          <button className={s.linkButton}>Color Game</button>
        </Link>
        <Link to="/doable" className={s.elementLink}>
          <button className={s.linkButton}>Doable</button>
        </Link>
      </div>
    </div>
  );
}

export default Home;
