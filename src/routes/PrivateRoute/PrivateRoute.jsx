
import { Navigate } from "react-router-dom";
import { useAuth } from "../../contexts/authContext";


const PrivateRoute = ({children}) => {
    const { isAuthenticated } = useAuth();

  return isAuthenticated ? children : <Navigate to="/doable" replace></Navigate>
}

export default PrivateRoute