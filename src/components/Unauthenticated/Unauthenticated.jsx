import s from "./Unauthenticated.module.css";

import { Form, useNavigate } from "react-router-dom"; 
import { useState } from "react";
import { useAuth } from "../../contexts/authContext";



function Unauthenticated() {
  const { login, signup } = useAuth(); 
  const navigate = useNavigate();

  const [status, setStatus] = useState("idle");
  const [activeTab, setActiveTab] = useState("login");
  const [signUpErrors, setSignUpErrors] = useState(null);


  async function handleSubmit(event) {
    event.preventDefault();

    const formData = new FormData(event.target);
    const email = formData.get("email");
    const password = formData.get("password");

    setStatus("loading");
    if(activeTab==='login'){
      try {
      await login(email, password);
      setStatus("success");
      navigate("/doable/notes"); 
    } catch (error) {
      setStatus("error");
      setSignUpErrors(error.message);
    }
    }else{
      try{
        await signup(email, password);
        setStatus("success");
        navigate("/doable/notes");
      }catch(error){
        setStatus("error");
        setSignUpErrors(error.message);
      }
    }
    
  }

  function handleTabChange(tab) {
    setActiveTab(tab);
    setStatus("idle");
  }

  const isLoading = status === "loading";
  const buttonText = activeTab === "login" ? "Enter" : "Create";
  const hasError = status === "error";


  return (
    <div className={s.wrapper}>
      <div className={s.tabs}>
        <button
          onClick={() => handleTabChange("login")}
          className={activeTab === "login" ? s.active : ""}
        >
          Login
        </button>
        <button
          onClick={() => handleTabChange("signup")}
          className={activeTab === "signup" ? s.active : ""}
        >
          Signup
        </button>
      </div>
      <Form className={s.form} method="POST" onSubmit={handleSubmit}>
        <div>
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            name="email"
            placeholder="user@example.com"
            required
          />
        </div>
        <div>
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            name="password"
            required
            minLength={6}
          />
        </div>
        <button type="submit" disabled={isLoading}>
          {isLoading ? "Loading..." : buttonText}
        </button>
      </Form>
      {hasError && (
        <p className={s["error-message"]}>
          {signUpErrors || "Invalid Credentials"}
        </p>
      )}
    </div>
  );
}

export default Unauthenticated;
