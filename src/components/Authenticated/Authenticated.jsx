import s from "./Authenticated.module.css";
import { BadgeAlert, Trash2 } from "lucide-react";
import { filterTasks, sortTasks } from "./utils";
import { useEffect, useState } from "react";

import { Form, useNavigate, useLoaderData, redirect } from "react-router-dom";
import { useAuth } from "../../contexts/authContext";
import {
  createTask,
  deleteTask,
  getTasks,
  editTask,
} from "../../services/tasks";
import { tokenKey } from "../../constants";

export async function loader() {
  const storedToken = localStorage.getItem(tokenKey);
  if (!storedToken) {
    return redirect("/doable");
  }
  const tasks = await getTasks();
  return { tasks };
}

function Authenticated() {
  const [status, setStatus] = useState("idle");
  const [formStatus, setFormStatus] = useState("idle");
  const [tasks, setTasks] = useState([]);
  const [selectedOption, setSelectedOption] = useState("");
  const [filters, setFilters] = useState({
    onlyPending: false,
    onlyImportant: false,
  });

  const { logout } = useAuth();
  const navigate = useNavigate();

  const { tasks: loadedTasks } = useLoaderData();
  console.log(loadedTasks);
  useEffect(() => {
    setTasks(loadedTasks);
  }, [loadedTasks]);

  async function handleSubmit(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    const taskData = Object.fromEntries(formData.entries());
    try {
      const createdTask = await createTask(taskData);
      setTasks((prevTasks) => [...prevTasks, createdTask]);
      event.target.title.value = "";
      event.target.due_date.value = "";
    } catch (error) {
      console.error("Error al crear tarea: ", error.message);
    }
  }

  async function handleEdit(id, updates) {
    try {
      await editTask(id, updates);
      const updateTask = tasks.map((task) => {
        if (task.id === id) {
          return {
            ...task,
            important: updates.important,
            completed: updates.completed,
          };
        }
        return task;
      });

      setTasks(updateTask);
    } catch (error) {
      console.error("Error en la actualizacion de una tarea: ", error.message);
    }
  }

  async function handleDelete(id) {
    try {
      await deleteTask(id);
      const newTask = tasks.filter((task) => task.id !== id);
      setTasks(newTask);
    } catch (error) {
      console.error("Error al eliminar una tarea: ", error.message);
    }
  }

  const handleSelectedOption = (event) => {
    setSelectedOption(event.target.value);
  };

  const isLoading = status === "loading";
  const isCreating = formStatus === "loading";

  const filteredTasks = filterTasks(tasks, filters);
  const sortedTasks = sortTasks(filteredTasks, selectedOption);

  return (
    <>
      <form className={s["task-form"]} onSubmit={handleSubmit}>
        <input
          id="title"
          type="text"
          name="title"
          placeholder="do the dishes"
          required
          aria-label="title"
          disabled={isCreating}
        />
        <input
          id="due_date"
          type="date"
          name="due_date"
          aria-label="due_date"
          disabled={isCreating}
        />
        <button disabled={isCreating}>
          {isCreating ? "Adding..." : "Add task"}
        </button>
      </form>

      <div className={s["tasks-wrapper"]}>
        <aside className={s.aside}>
          <div className={s["input-group"]}>
            <label htmlFor="sort_by">Sort by</label>
            <select
              id="sort_by"
              value={selectedOption}
              onChange={handleSelectedOption}
            >
              <option value="due_date-asc">Due Date (old first)</option>
              <option value="due_date-desc">Due Date (new first)</option>
              <option value="alphabetical-asc">Alphabetical (a-z)</option>
              <option value="alphabetical-desc">Alphabetical (z-a)</option>
            </select>
          </div>
          <div className={s["input-group"]}>
            <label>Filter</label>
            <div className={s.checkbox}>
              <input
                type="checkbox"
                id="pending"
                checked={filters.onlyPending}
                onChange={(e) =>
                  setFilters({ ...filters, onlyPending: e.target.checked })
                }
              />
              <label htmlFor="pending">Only pending</label>
            </div>
            <div className={s.checkbox}>
              <input
                type="checkbox"
                id="important"
                checked={filters.onlyImportant}
                onChange={(e) =>
                  setFilters({ ...filters, onlyImportant: e.target.checked })
                }
              />
              <label htmlFor="important">Only important</label>
            </div>
          </div>
          <button
            onClick={() => {
              logout();
              navigate("/doable");
            }}
          >
            Logout
          </button>
        </aside>
        <div className={s["tasks-list"]}>
          {isLoading && <p>Loading...</p>}
          {tasks.length > 0 &&
            sortedTasks.map((task) => (
              <div key={task.id} className={s["task-wrapper"]}>
                <div className={s["task-data"]}>
                  <input
                    type="checkbox"
                    id={task.id}
                    checked={task.completed}
                    onChange={() => {
                      handleEdit(task.id, {
                        important: task.important,
                        completed: !task.completed,
                      });
                    }}
                  />
                  <div className={s["title-wrapper"]}>
                    <label htmlFor={task.id} className={s["task-title"]}>
                      {task.title}
                    </label>
                    <small className={s["task-due_date"]}>
                      {task["due_date"]}
                    </small>
                  </div>
                </div>
                <div className={s.actions}>
                  <button
                    onClick={() => {
                      handleEdit(task.id, {
                        important: !task.important,
                        completed: task.completed,
                      });
                    }}
                  >
                    <BadgeAlert />
                  </button>
                  <button
                    onClick={() => {
                      handleDelete(task.id);
                    }}
                  >
                    <Trash2 />
                  </button>
                </div>
              </div>
            ))}
        </div>
      </div>
    </>
  );
}

export default Authenticated;
