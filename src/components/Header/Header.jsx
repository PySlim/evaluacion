import s from './Header.module.css';

import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <header className={s.custonHeader}>
      <div className={s.custonHeader__logo}>
        <div >
          <img src="/assets/react-icon.svg" alt="" width={32}  height={32}/>
        </div>
        <div className={s.tipografiaUno}>
          <ul className={s.custonHeader__logoUl}>
            <li>
              <Link to="/" className={s.link}>React Evaluation</Link>
            </li>
          </ul>
        </div>
      </div>
      <div className={s.custonHeader__logo}>

        <ul className={s.custonHeader__logoUl}>
          <li className={s.tipografiaDos}>
            <Link to="color-game" className={s.link}>Color Game</Link>
          </li>
          <li className={s.tipografiaDos}>
            <Link to="doable"className={s.link}>Doable</Link>
          </li>
        </ul>

      </div>
      
    </header>
  )
}

export default Header